FROM debian:stable-slim

RUN apt-get update -q && \
    apt-get install -y -qq --no-install-recommends \
        ca-certificates  \
        curl \
        ghostscript \
        git \
        gnuplot \
        imagemagick \
        make \
        jq \
        qpdf \
        python3-pygments \
        wget \
        texlive-full \
        vim-tiny && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /data

VOLUME ["/data"]
